<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"  xml:lang="pt-br" lang="pt-br">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="pt-br" />

        <title>PHP ZCE Study</title>
    </head>
    <body>
        <h1>PHP ZCE Study</h1>

        <?php

        $directorys = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(__DIR__));

        foreach($directorys AS $directory) {
            if(substr($directory , -4) === '.php' && $directory != __FILE__) {
                ob_start();

                require($directory);

                $result = ob_get_contents();
                ob_end_clean();

                printf(
                    '<div>
                        <h2>%s</h2>
                        <h3>Code</h3>
                        %s

                        <h3>Result</h3>
                        %s
                    </div>
                    <hr />
                    ',
                    $directory,
                    highlight_string(file_get_contents($directory) , true),
                    $result
                );
            }
        }
        ?>
    </body>
</html>